/* Copyright (c) 2010, James Burke <james@jabsys.net> 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "libctpool.h"

#define POOL_KILL 0
#define POOL_OK 1
#define POOL_PAUSE 2

#ifdef DEBUG
#define debug_print(fmt, ...) printf(fmt, __VA_ARGS__)
#else
#define debug_print(fmt, ...) do {} while (0)
#endif

struct job_t {
    struct job_t *next;
    void *data;
    void (*func)(void *data);
    int priority;
};

struct thread_pool {
    struct job_t *first_job;
    struct job_t *last_job;
    int max_threads;
    int min_threads;
    int threads;
    int jobs;
    int status;
    int scheduler;
    pthread_mutex_t pool_mutex;
    pthread_mutex_t job_mutex;
    pthread_cond_t job_signal;
};

static void *job_thread(void *ptr);
static int int_priority_job(thread_pool_t *pool_id, struct job_t *jc);
static int int_fifo_job(thread_pool_t *pool_id, struct job_t *jc);
static int int_lifo_job(thread_pool_t *pool_id, struct job_t *jc);

int ctp_create_pool(thread_pool_t **pool_id, int min_threads, int max_threads, int mode)
{
    /* create & fill in thread pool structure */

    *pool_id = NULL;
    *pool_id = (thread_pool_t *)malloc(sizeof(thread_pool_t));
    if (*pool_id == NULL) return POOL_CREATE_ERR;

    (*pool_id)->min_threads = min_threads;
    (*pool_id)->max_threads = max_threads;
    (*pool_id)->threads = 0;
    (*pool_id)->status = POOL_OK;
    (*pool_id)->scheduler = mode;

    if (pthread_mutex_init(&(*pool_id)->pool_mutex, NULL)) return POOL_CREATE_ERR;
    if (pthread_mutex_init(&(*pool_id)->job_mutex, NULL)) return POOL_CREATE_ERR;
    if (pthread_cond_init(&(*pool_id)->job_signal, NULL)) return POOL_CREATE_ERR;

    printf("pool mode %i \n", mode);

    return(0);
}

int ctp_add_job(thread_pool_t *pool_id, int priority, void(*function)(void *data), void *data)
{
    if (pool_id == NULL) return POOL_DEAD;

    /* create the new job card */

    if (pool_id->status == POOL_KILL) return JOB_CREATE_ERR;

    struct job_t *jc;
    jc = (struct job_t *)malloc(sizeof(struct job_t));
    if(!jc) return JOB_CREATE_ERR;
    jc->data = data;
    jc->func = function;
    jc->next = NULL;
    jc->priority = priority;

    /* add the job */
    
    if (pool_id->scheduler == CTP_PRIORITY) {
        int_priority_job(pool_id, jc);
    } else if (pool_id->scheduler == CTP_FIFO) {
        int_fifo_job(pool_id, jc);
    } else {
        int_lifo_job(pool_id, jc);
    }
    
    /* check if there's enough threads */
 
    pthread_mutex_lock(&pool_id->pool_mutex);
    pool_id->jobs++;
    if ( (pool_id->jobs > pool_id->threads) && (pool_id->threads < pool_id->max_threads) ) {
        pthread_t pid;
        pthread_create(&pid, NULL, job_thread, pool_id);        
        pool_id->threads++;
        debug_print("created an extra thread : jobs %i : threads %i : max %i \n",
                    pool_id->threads, pool_id->jobs, pool_id->max_threads);
    }

    pthread_mutex_unlock(&pool_id->pool_mutex);
    pthread_cond_signal(&pool_id->job_signal);
    debug_print("created job %x\n", jc);

    return(0);
}

int ctp_kill_pool(thread_pool_t **pool_id)
{

    if (pool_id == NULL) return POOL_DEAD;

    (*pool_id)->status = POOL_KILL;
    pthread_cond_broadcast(&(*pool_id)->job_signal);

    /* wait for all the threads to die */

    while ( (*pool_id)->threads != 0 ) {
        sleep(1);
    }

    /* clean up any jobs left */

    struct job_t *jq;
    jq = (*pool_id)->first_job;
    while (jq != NULL) {
		free(jq->data);
        free(jq);
    	jq = jq->next;
    }

    /* free the pool */

    free(*pool_id);
    *pool_id = NULL;

    return(0);
}

static int int_fifo_job(thread_pool_t *pool_id, struct job_t *jc)
{
    struct job_t *jq;

    debug_print("adding %x to list - ", jc);

    pthread_mutex_lock(&pool_id->job_mutex);
    if (pool_id->last_job == NULL) {
        pool_id->first_job = pool_id->last_job = jc;
    } else {
        pool_id->last_job->next = jc;
        pool_id->last_job = jc;
    }
    pthread_mutex_unlock(&pool_id->job_mutex);

    return(0);
}

static int int_lifo_job(thread_pool_t *pool_id, struct job_t *jc)
{
    struct job_t *jq;

    debug_print("adding %x to list - ", jc);

    pthread_mutex_lock(&pool_id->job_mutex);
    if (pool_id->last_job == NULL) {
        pool_id->first_job = pool_id->last_job = jc;
    } else {
        jc->next = pool_id->first_job;
        pool_id->first_job = jc;
    }
    pthread_mutex_unlock(&pool_id->job_mutex);

    return(0);
}

static int int_priority_job(thread_pool_t *pool_id, struct job_t *jc)
{

    struct job_t *jq;

    debug_print("adding %x to list prio %i - ", jc, jc->priority);
    pthread_mutex_lock(&pool_id->job_mutex);

    jq = pool_id->first_job;
    if (pool_id->first_job == NULL) {                        /* only job */
        pool_id->first_job = pool_id->last_job = jc;
        pthread_mutex_unlock(&pool_id->job_mutex);
        debug_print("added first %i\n", NULL);
        return(0);
        }

    if (pool_id->first_job->priority > jc->priority) {         /* higher priority than first job */
        jc->next = pool_id->first_job;
        pool_id->first_job = jc;
        pthread_mutex_unlock(&pool_id->job_mutex);
        debug_print("added before %x \n", jc->next);
        return(0);
        }

    if (pool_id->last_job->priority < jc->priority) {       /* lower priority than last job */
        pool_id->last_job->next = jc;
        pool_id->last_job = jc;
        pthread_mutex_unlock(&pool_id->job_mutex);
        debug_print("added last after %x \n", jq);
        return(0);
        }

    /* need to find where we go in the que */

    while (jq->next != NULL) {
        if (jc->priority < jq->next->priority) {
            jc->next = jq->next;
            jq->next = jc;
            pthread_mutex_unlock(&pool_id->job_mutex);
            debug_print("added between %x & %x \n", jc->next, jq);
            return(0);
            }
        jq = jq->next;
    }

    if (jq->next == NULL) {         /* last option, must be last */
        jq->next = jc;
        debug_print("added last after %x \n", pool_id->last_job);
        pool_id->last_job = jc;
        pthread_mutex_unlock(&pool_id->job_mutex);
        return(0);
    }

    return(0);
}

int ctp_pause_pool(thread_pool_t *pool_id)
{
    if (pool_id == NULL) return POOL_DEAD;
    pthread_mutex_lock(&pool_id->pool_mutex);
    pool_id->status = POOL_PAUSE;
    pthread_mutex_unlock(&pool_id->pool_mutex);
    debug_print("paused pool %x \n", pool_id);
    return(0);
}

int ctp_start_pool(thread_pool_t *pool_id)
{
    if (pool_id == NULL) return POOL_DEAD;
    pthread_mutex_lock(&pool_id->pool_mutex);
    pool_id->status = POOL_OK;
    pthread_mutex_unlock(&pool_id->pool_mutex);
    debug_print("started pool %x \n", pool_id);
    return(0);
}

int ctp_max_threads(thread_pool_t *pool_id, int threads)
{
    if (pool_id == NULL) return POOL_DEAD;
    pthread_mutex_lock(&pool_id->pool_mutex);
    pool_id->max_threads = threads;
    debug_print("changed max thread of pool %x to %i \n", pool_id, pool_id->max_threads);
    pthread_mutex_unlock(&pool_id->pool_mutex);
    return(0);
}

int ctp_min_threads(thread_pool_t *pool_id, int threads)
{
    if (pool_id == NULL) return POOL_DEAD;
    pthread_mutex_lock(&pool_id->pool_mutex);
    pool_id->min_threads = threads;
    debug_print("changed min thread of pool %x to %i \n", pool_id, pool_id->min_threads);
    pthread_mutex_unlock(&pool_id->pool_mutex);
    return(0);
}


static void *job_thread(void *ptr)
{

    /* sit in loops & do our jobs */

    thread_pool_t *pool_id = (thread_pool_t *)ptr;
    struct job_t *jq;
    
    for (;;) {
        pthread_mutex_lock(&pool_id->job_mutex);
        while ((pool_id->first_job == NULL) && (pool_id->status == POOL_OK)) {
            pthread_cond_wait(&pool_id->job_signal, &pool_id->job_mutex);
        }

        if (pool_id->status == POOL_PAUSE) {
            pthread_mutex_unlock(&pool_id->job_mutex);
        } 

        else if (pool_id->status == POOL_KILL) {
            pthread_mutex_unlock(&pool_id->job_mutex);
            pool_id->threads--;
            debug_print("thread %x dieing\n", pthread_self());
            pthread_exit(NULL);
        } else {

            jq = pool_id->first_job;
        
            if (jq->next == NULL) {
                pool_id->first_job = NULL;
                pool_id->last_job = NULL;
            } else {
                pool_id->first_job = jq->next;
            }
            pthread_mutex_unlock(&pool_id->job_mutex);

            (*jq->func)(jq->data);          /* run the job */
            free(jq);
        
            pthread_mutex_lock(&pool_id->pool_mutex);
            pool_id->jobs--;
            debug_print("jobs left %i\n", pool_id->jobs);

            /* check if we are still needed */
            if( (pool_id->threads > pool_id->jobs) && (pool_id->threads > pool_id->min_threads )) {
                pool_id->threads--;
                debug_print("no longer needed killing thread %x : threads left %i jobs left %i  \n",
                                                        pthread_self(), pool_id->threads, pool_id->jobs); 
                pthread_mutex_unlock(&pool_id->pool_mutex);
                pthread_exit(NULL);
            }
            pthread_mutex_unlock(&pool_id->pool_mutex);
        }
    }
}

