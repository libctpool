/* Copyright (c) 2010, James Burke <james@jabsys.net> 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __LIBCTPOOL_H
#define __LIBCTPOOL_H

typedef struct thread_pool thread_pool_t;         // Users never need directly modify the structure 

// error code

#define POOL_CREATE_ERR 1
#define JOB_CREATE_ERR 2
#define POOL_DEAD 3

// scheduler modes

#define CTP_PRIORITY 20
#define CTP_FIFO 21
#define CTP_LIFO 22

extern int ctp_create_pool(thread_pool_t **pool_id, int min_threads, int max_threads, int mode);
extern int ctp_add_job(thread_pool_t *pool_id, int priority, void(*function)(void *data), void *data);
extern int ctp_kill_pool(thread_pool_t **pool_id);
extern int ctp_pause_pool(thread_pool_t *pood_id);
extern int ctp_start_pool(thread_pool_t *pood_id);
extern int ctp_max_threads(thread_pool_t *pool_id, int threads);
extern int ctp_min_threads(thread_pool_t *pool_id, int threads);

#endif
