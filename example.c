/* Copyright (c) 2010, James Burke <james@jabsys.net> 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "libctpool.h"

/* max & min threads */

#define MIN_THREADS 3
#define MAX_THREADS 8

/* sample job & the jobs data */

void sample_job(void *data); 

struct jobd{
    int a;
};

int main (int argc, char *argv[]) 
{
    /* data structure for the pool, ctp_create_pool allocates the memory */

    thread_pool_t *pool_id;                            
    
    /* create the pool */
    
    if (ctp_create_pool(&pool_id, MIN_THREADS, MAX_THREADS, CTP_PRIORITY) == POOL_CREATE_ERR) { 
        printf("pool creation failed \n");
        exit(1); 
    }
    printf("created thread pool %x\n", pool_id);

    /* pause the pool */

    printf("pausing the pool\n");
    ctp_pause_pool(pool_id);
     
    /* create some jobs, done backwards so ctp_add_pool can sort them */

    int i;
    struct jobd *jd;
    for (i=20;i!=1;i--) {
        jd = (struct jobd *)malloc(sizeof(struct jobd));
        jd->a = i;
        if (ctp_add_job(pool_id, i, &sample_job, jd) == JOB_CREATE_ERR) {
            printf("failed to add job %i to pool %x \n", jd->a, pool_id);
            } else {
            printf("added job %i to pool %x \n", jd->a, pool_id);
        }
    }

    /* change max threads */

    ctp_max_threads(pool_id, 15);
    
    /* change mix threads */

    ctp_min_threads(pool_id, 1);

    sleep(5);

   /* run the jobs */

    printf("running the jobs\n");
    ctp_start_pool(pool_id);

    /* give the jobs time to finsh */
    
    sleep(10);

    /* kill the pool */

    ctp_kill_pool(&pool_id);
    printf("pool id is %x \n", pool_id);
 
    for (;;) {
        sleep(1);           /* just sit & loop for now */
    }

    exit(0);
}

/* sample function */

void sample_job(void *data)
{
    struct jobd *jd;
    jd = data;          /* save casting it all the time */
    
    printf("I'm thread %x priority %i \n", pthread_self(), jd->a);
    sleep(1);
    printf("job %i done by thread %x \n", jd->a, pthread_self());
    
    free(jd);           /* free the memory we used */
}
