/* Copyright (c) 2010, James Burke <james@jabsys.net> 
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "libctpool.h"

#define MIN_THREADS 3
#define MAX_THREADS 6

void sample_job(void *data);

struct jobd{
    int a;
};

struct thread_pool {
    struct job_t *first_job;
    struct job_t *last_job;
    int max_threads;
    int min_threads;
    int threads;
    int jobs;
    int status;
    int scheduler;
    pthread_mutex_t pool_mutex;
    pthread_mutex_t job_mutex;
    pthread_cond_t job_signal;
};

struct job_t {
    struct job_t *next;
    void *data;
    void (*func)(void *data);
    int priority;
};

int main (int argc, char *argv[]) 
{ 
    int test = atoi(argv[1]);
    int ret;
    struct thread_pool *pool_id;

    if (test == 1) { ret = ctp_create_pool(&pool_id, MIN_THREADS, MAX_THREADS, CTP_PRIORITY); } else
    if (test == 2) { ret = ctp_create_pool(&pool_id, MIN_THREADS, MAX_THREADS, CTP_FIFO); } else
    if (test == 3) { ret = ctp_create_pool(&pool_id, MIN_THREADS, MAX_THREADS, CTP_LIFO); } else 
    { ret = ctp_create_pool(&pool_id, MIN_THREADS, MAX_THREADS, CTP_FIFO); }
    
    int i;
    int ar[] = {5,8,3,9,2,6,1,4,7,0};

    ctp_pause_pool(pool_id);

    struct jobd *jd;
    struct job_t *jq;

    for (i=0 ; i != 10 ; i++) {
        jd = (struct jobd *)malloc(sizeof(struct jobd));
        jd->a = ar[i];
        ctp_add_job(pool_id, ar[i], &sample_job, jd);    
    }

    if (test == 1 || test == 2 || test == 3) {
        printf("\n");
        jq = pool_id->first_job;
        while(jq != NULL) {
            printf("%i", jq->priority);
            jq = jq->next;
        }
        printf("\n");
    }

    exit(0);
}

void sample_job(void *data)
{
    struct jobd *jd;
    jd = data;          /* save casting it all the time */
    sleep(1);
    free(jd);           /* free the memory we used */
}

